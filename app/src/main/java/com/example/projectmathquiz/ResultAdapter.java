package com.example.projectmathquiz;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projectmathquiz.model.QuizResult;

import java.util.ArrayList;

import androidx.annotation.DrawableRes;

class ResultListAdapter extends BaseAdapter {
    Activity context;
    ArrayList<QuizResult> quizResults;
    private static LayoutInflater inflater = null;

    public ResultListAdapter(Activity context, ArrayList<QuizResult> quizResults) {
        this.context = context;
        this.quizResults = quizResults;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return quizResults.size();
    }

    @Override
    public QuizResult getItem(int position) {
        return quizResults.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = convertView;
        itemView = (itemView == null) ? inflater.inflate(R.layout.row, null) : itemView;
        QuizResult oneQuizResult = quizResults.get(position);
        TextView tvRowQuiz = (TextView) itemView.findViewById(R.id.tv_RowQuiz);
        tvRowQuiz.setText(oneQuizResult.getQuiz());
        TextView tvRowCorrectAnswer = (TextView) itemView.findViewById(R.id.tv_RowCorrectAnswer);
        tvRowCorrectAnswer.setText(String.valueOf(oneQuizResult.getCorrectAnswer()));
        TextView tvRowUserAnswer = (TextView) itemView.findViewById(R.id.tv_RowUserAnswer);
        tvRowUserAnswer.setText(String.valueOf(oneQuizResult.getUserAnswer()));
        ImageView ivRowIfRight = (ImageView) itemView.findViewById(R.id.iv_RowIfRight);
        ivRowIfRight.setImageResource(oneQuizResult.isIfRight() == true ? R.drawable.ic_check_circle_solid : R.drawable.ic_times_circle_solid);
        return itemView;
    }

    public void updateDate(ArrayList<QuizResult> newResultList) {
        quizResults = newResultList;
    }
}