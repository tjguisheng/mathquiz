package com.example.projectmathquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.projectmathquiz.model.QuizResult;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ActivityResult extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    RadioGroup rg_Sort;
    Button btn_Back;
    EditText et_UserName;
    TextView tv_Score;
    ListView lv_ResultList;
    ArrayList<QuizResult> quizResultArrayList, quizResultArrayListRight, quizResultArrayListWrong;

    float score;
    String userName;
    ResultListAdapter quizResultArrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        initialize();
    }


    private void initialize() {
        rg_Sort = findViewById(R.id.rg_result);
        rg_Sort.setOnCheckedChangeListener(this);
        et_UserName = findViewById(R.id.et_Name);
        tv_Score = findViewById(R.id.tv_Score);
        btn_Back = findViewById(R.id.btn_Back);
        btn_Back.setOnClickListener(this);
        lv_ResultList = findViewById(R.id.lv_ResultList);

        Intent intent = getIntent();
        if (intent.hasExtra("userName")) {
            userName = intent.getStringExtra("userName");
            et_UserName.setText(userName);
        }

        Bundle bundle = intent.getBundleExtra("intentExtra");

        Serializable serializable = bundle.getSerializable("quizResultArrayList");
        quizResultArrayList = (ArrayList<QuizResult>) serializable;

        //separate right and wrong answer
        quizResultArrayListRight = new ArrayList<>();
        quizResultArrayListWrong = new ArrayList<>();
        for (QuizResult result : quizResultArrayList) {
            if (result.isIfRight()) {
                quizResultArrayListRight.add(result);
            } else {
                quizResultArrayListWrong.add(result);
            }

        }

        //set score
        if (!quizResultArrayList.isEmpty()) {
            score = (float) quizResultArrayListRight.size() / quizResultArrayList.size();
            score = Math.round(score * 100) / 100f;
        } else {
            score = 0f;
        }
        tv_Score.setText(String.valueOf(score * 100) + "%");

        //set ListAdapter
        quizResultArrayAdapter = new ResultListAdapter(this, quizResultArrayList);
        //add header
        ViewGroup headerView = (ViewGroup) getLayoutInflater().inflate(R.layout.header, lv_ResultList, false);

        lv_ResultList.setAdapter(quizResultArrayAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Back:
                backToMain();
                break;
        }
    }


    private void backToMain() {
        Intent intent = new Intent();
        String userName = et_UserName.getText().toString();
        if (!userName.equals("")) {
            intent.putExtra("userName", userName);
            intent.putExtra("score", score);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_CANCELED, intent);
        }
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int selectedRadioBtn = rg_Sort.getCheckedRadioButtonId();
        switch (selectedRadioBtn) {
            case R.id.rb_All:
                quizResultArrayAdapter.quizResults = quizResultArrayList;
                quizResultArrayAdapter.notifyDataSetChanged();
                break;
            case R.id.rb_Right:
                quizResultArrayAdapter.quizResults = quizResultArrayListRight;
                quizResultArrayAdapter.notifyDataSetChanged();
                break;
            case R.id.rb_Wrong:
                quizResultArrayAdapter.quizResults = quizResultArrayListWrong;
                quizResultArrayAdapter.notifyDataSetChanged();
                break;
            case R.id.rb_SortA:
                Collections.sort(quizResultArrayList);
                quizResultArrayAdapter.quizResults = quizResultArrayList;
                quizResultArrayAdapter.notifyDataSetChanged();
                break;
            case R.id.rb_SortD:
                Collections.sort(quizResultArrayList, Collections.<QuizResult>reverseOrder());
                quizResultArrayAdapter.quizResults = quizResultArrayList;
                quizResultArrayAdapter.notifyDataSetChanged();
        }
    }
}