package com.example.projectmathquiz;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmathquiz.model.QuizResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    int firstNum, secondNum;
    float correctAnswer;
    Operation operation;
    String onScreen, quiz, userName;
    ArrayList<QuizResult> quizResultArrayList;
    final static int REQUEST_CODE1 = 1;

    TextView tv_Answer, tv_Quiz, tv_Title;
    Button btn_One, btn_Two, btn_Three, btn_Four, btn_Five, btn_Six, btn_Seven, btn_Eight, btn_Nine, btn_Zero;
    Button btn_Dot, btn_Negative, btn_Generate, btn_Validate, btn_Clear, btn_Score, btn_Finish;

    enum Operation {
        PLUS,
        MINUS,
        MULTIPLY,
        DIVIDE;
        private static final List<Operation> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
        private static final int SIZE = VALUES.size();
        private static final Random RANDOM = new Random();

        public static Operation randomOperation() {
            return VALUES.get(RANDOM.nextInt(SIZE));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Initialize();
    }

    private void Initialize() {
        tv_Answer = findViewById(R.id.tv_Answer);
        tv_Quiz = findViewById(R.id.tv_Quiz);

        btn_One = findViewById(R.id.btn_One);
        btn_One.setOnClickListener(this);
        btn_Two = findViewById(R.id.btn_Two);
        btn_Two.setOnClickListener(this);
        btn_Three = findViewById(R.id.btn_Three);
        btn_Three.setOnClickListener(this);
        btn_Four = findViewById(R.id.btn_Four);
        btn_Four.setOnClickListener(this);
        btn_Five = findViewById(R.id.btn_Five);
        btn_Five.setOnClickListener(this);
        btn_Six = findViewById(R.id.btn_Six);
        btn_Six.setOnClickListener(this);
        btn_Seven = findViewById(R.id.btn_Seven);
        btn_Seven.setOnClickListener(this);
        btn_Eight = findViewById(R.id.btn_Eight);
        btn_Eight.setOnClickListener(this);
        btn_Nine = findViewById(R.id.btn_Nine);
        btn_Nine.setOnClickListener(this);
        btn_Zero = findViewById(R.id.btn_Zero);
        btn_Zero.setOnClickListener(this);
        btn_Dot = findViewById(R.id.btn_Dot);
        btn_Dot.setOnClickListener(this);
        btn_Negative = findViewById(R.id.btn_Negative);
        btn_Negative.setOnClickListener(this);
        btn_Clear = findViewById(R.id.btn_Clear);
        btn_Clear.setOnClickListener(this);
        btn_Generate = findViewById(R.id.btn_Generate);
        btn_Generate.setOnClickListener(this);
        btn_Validate = findViewById(R.id.btn_Validate);
        btn_Validate.setOnClickListener(this);
        btn_Clear = findViewById(R.id.btn_Clear);
        btn_Clear.setOnClickListener(this);
        btn_Score = findViewById(R.id.btn_Score);
        btn_Score.setOnClickListener(this);
        btn_Finish = findViewById(R.id.btn_Finish);
        btn_Finish.setOnClickListener(this);

        tv_Title = findViewById(R.id.tv_Title);
        quizResultArrayList = new ArrayList<>();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_One:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 1;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Two:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 2;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Three:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 3;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Four:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 4;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Five:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 5;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Six:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 6;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Seven:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 7;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Eight:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 8;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Nine:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 9;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Zero:
                onScreen = tv_Answer.getText().toString();
                if (onScreen.equals("0")) {
                    onScreen = "";
                }
                onScreen += 0;
                tv_Answer.setText(onScreen);
                break;
            case R.id.btn_Dot:
                onScreen = tv_Answer.getText().toString();
                if (!onScreen.contains(".")) {
                    if (onScreen.equals("")) {
                        onScreen = "0.";
                    } else {
                        onScreen += ".";
                    }
                    tv_Answer.setText(onScreen);
                }
                break;
            case R.id.btn_Negative:
                onScreen = tv_Answer.getText().toString();
                if (!onScreen.equals("0")) {
                    if (onScreen.contains("-")) {
                        onScreen = onScreen.substring(1);
                    } else {
                        onScreen = "-" + onScreen;
                    }
                }
                tv_Answer.setText(onScreen);
                break;

            case R.id.btn_Generate:
                generateQuiz();
                break;
            case R.id.btn_Validate:
                validateAnswer();
                break;
            case R.id.btn_Clear:
                clearAnswer();
                break;
            case R.id.btn_Score:
                gotoResult();
                break;
            case R.id.btn_Finish:
                finish();
        }
    }


    private void generateQuiz() {
        Random random = new Random();
        firstNum = random.nextInt(11);
        secondNum = random.nextInt(11);
        operation = operation.randomOperation();

        switch (operation) {
            case PLUS:
                quiz = firstNum + " + " + secondNum;
                tv_Quiz.setText(quiz);
                correctAnswer = firstNum + secondNum;
                break;
            case MINUS:
                quiz = firstNum + " - " + secondNum;
                tv_Quiz.setText(quiz);
                correctAnswer = firstNum - secondNum;
                break;
            case MULTIPLY:
                quiz = firstNum + " * " + secondNum;
                tv_Quiz.setText(quiz);
                correctAnswer = firstNum * secondNum;
                break;
            case DIVIDE:
                while (secondNum == 0) {
                    secondNum = random.nextInt(11);
                }
                quiz = firstNum + " / " + secondNum;
                tv_Quiz.setText(quiz);
                correctAnswer = Math.round(firstNum / secondNum * 100) / 100;
                break;
        }
    }

    private void validateAnswer() {
        if (!tv_Answer.getText().toString().equals("")) {
            //get user answer
            float answer = Float.parseFloat(tv_Answer.getText().toString());
            //generate quizResult by quiz and userAnswer
            QuizResult quizResult = new QuizResult(quiz, correctAnswer, answer, (correctAnswer == answer));
            quizResultArrayList.add(quizResult);
            //Toast result
            Toast.makeText(this, quizResult.isIfRight() ? "Right" : "Wrong", Toast.LENGTH_LONG).show();
            //generate a new quiz and clear userAnswer
            generateQuiz();
            clearAnswer();
        }
    }

    private void clearAnswer() {
        tv_Answer.setText("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE1) {
            userName = (String) data.getStringExtra("userName");
            float score = (float) data.getFloatExtra("score",2);
            if (resultCode == RESULT_OK) {
                tv_Title.setText(userName + " Score: " + (score * 100) + "%");
            } else {
                tv_Title.setText("Math Quiz");
            }
        }
    }

    private void gotoResult() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("quizResultArrayList", quizResultArrayList);
        Intent intent = new Intent(this, ActivityResult.class);
        intent.putExtra("intentExtra", bundle);
        if (userName != "") {
            intent.putExtra("userName", userName);
        }
        startActivityForResult(intent, REQUEST_CODE1);
    }
}
