package com.example.projectmathquiz.model;

import java.io.Serializable;
import java.util.Objects;

import androidx.annotation.NonNull;

public class QuizResult implements Comparable, Serializable {
    private String quiz;
    private float correctAnswer, userAnswer;
    private boolean ifRight;

    public QuizResult(String quiz, float correctAnswer, float userAnswer, boolean ifRight) {
        this.quiz = quiz;
        this.correctAnswer = correctAnswer;
        this.userAnswer = userAnswer;
        this.ifRight = ifRight;
    }

    public String getQuiz() {
        return quiz;
    }

    public void setQuiz(String quiz) {
        this.quiz = quiz;
    }

    public float getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(float correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public float getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(float userAnswer) {
        this.userAnswer = userAnswer;
    }

    public boolean isIfRight() {
        return ifRight;
    }

    public void setIfRight(boolean ifRight) {
        this.ifRight = ifRight;
    }


    @Override
    public int compareTo(@NonNull Object o) {
        QuizResult otherObject = (QuizResult) o;
        if(ifRight==otherObject.isIfRight()){
            return 0;
        }else if(ifRight==true&otherObject.isIfRight()==false){
            return 1;
        }
        return -1;
    }
}
